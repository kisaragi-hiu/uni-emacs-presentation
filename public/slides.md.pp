#lang pollen

# ![Emacs logo](static/emacs-logo.png) GNU Emacs

◊img["static/emacs-default.png"]{screenshot}

.footnote[古尚賢, 林柏懷, 沈煜翔, 蔡永倫, 陳致澔]

---

# Text is multimedia

Text is the simplest medium thoughts can live on.

Text processing is multimedia.

---

# Emacs history (1/2)

## TECO
- 1972: Control-R mode (real time screen display; gives you a sense of how long ago we're talking about)
- 1976: Macros -> EMACS (Extended MACroS)

## Emacs
- Various Emacs flavors
- 1978: Multics Emacs: Emacs in a Lisp language
- 1984: GNU Emacs
- 1990s: LucidEmacs -> XEmacs
- 2000s: XEmacs died out

---

# Emacs history (2/2)

GNU Emacs is still actively developed.

Version 26.1 released on 2018-05-28.

◊img["static/emacs-site.png"]{new site}

---

# Emacs showcase: Dired (1/3)

Text-based file manager in the Emacs interface.

- Navigate files and folders
- Mark files for deletion
- Enter read-write mode and edit file names like text

*First version made in 1974, literally the first file manager in history ever written.*

---

# Emacs showcase: Dired (2/3)

Text-based file manager in the Emacs interface.

- Mark files for deletion

◊video["static/emacs-dired-delete-files.mp4"]

???

1. File → Open directory: open a folder (in Dired)
2. Move cursor with arrow keys (or mouse)
3. Press d to mark highlighted file for deletion
4. Press u to unmark
5. Press x to actually delete files

---

# Emacs showcase: Dired (3/3)

Text-based file manager in the Emacs interface.

- Enter read-write mode and edit file names like text

◊video["static/emacs-dired-read-write.mp4"]

???

- Press `Menu -> Immediate -> Edit File Names...`
  Then edit file names as if they're part of a text file.

After editing, press `C-c C-c` (Control-c twice) to save the changes to the folder.

---

# Emacs showcase: Org (1/4)

Powerful notebook markup format and tools for taking notes.

- Create headings and fold them with TAB.
- Lists, Checkboxes, TODOs, code-blocks

---

# Emacs showcase: Org (2/4)

Powerful notebook markup format and tools for taking notes.

- Create headings and fold them with TAB.

◊video["static/emacs-org-heading.mp4"]

---

# Emacs showcase: Org (3/4)

Powerful notebook markup format and tools for taking notes.

- Lists, Checkboxes

◊video["static/emacs-org-list.mp4"]

---

# Emacs showcase: Org (4/4)

Powerful notebook markup format and tools for taking notes.

- TODOs with headings

◊video["static/emacs-org-todo.mp4"]

---

# Emacs showcase: Customization

.greyed[*To Emacs users: yes this ignores the existence of `Customize`.*]

Emacs can be customized by simply setting variables.

The full power of a programming language is available at anytime as well.

```elisp
(setq delete-by-moving-to-trash t
      require-final-newline t
      ring-bell-function 'ignore)

(setq-default indent-tabs-mode nil
              show-trailing-whitespace t
              display-line-numbers t)
```

???

This tells Emacs to, line by line:
- use the trash bin when deleting files
- always make sure there's a "newline" character at the end of a file
- never ring the "bell"
- indent text with spaces instead of the "tab" character
- highlight spaces and tabs at the end of a line
- show line numbers next to a pane

---

# Emacs showcase: Help system

Emacs has a lot of help functions. Check them out in the `help` menu.

◊video["static/emacs-help.mp4"]

???

Press
- `C-h k` to describe a key combo,
- `C-h f` to describe a function, and
- `C-h v` to describe a variable.

"What function does pressing Backspace run?"
=> `C-h k Backspace` to show. It's `backward-delete-char-untabify`.

`Menu -> File -> Save` says it's bound to `C-x C-s`. What is the actual function?
=> Press `C-h k C-x C-s` to show. It's `save-buffer`.

What is a "Buffer"?
=> Press `C-h r`, then search for "Buffer" by typing `C-s buffer`. Repeat `C-s` until you find the subject "Buffers". Left click into it.

(BTW, `C-h k C-x C-s` means Control-h (release) k (release) Control-x (release) Control-s.)

---

# Thank you

## Resources
- Emacs homepage: https://www.gnu.org/software/emacs/
- Xah Lee's Practical Emacs Tutorial: http://ergoemacs.org/emacs/emacs.html
