# GNU Emacs Presentation

Made for a college report, "introduce a multimedia application".

# Project structure

In `public`:

- `index.html`: Main presentation file. Using [Remark.js](https://remarkjs.com/).
- `styles.css`: CSS for `index.html`.
- `slides.md.pp`: Pollen preprocessor file for generating `slides.md`.
- `pollen.rkt`: Pollen function / variable definitions.
- `.gitlab-ci.yml`: installs Pollen and renders `slides.md.pp` in Gitlab CI.

- `.editorconfig`: [EditorConfig](https://editorconfig.org/)
- `static/`: resources used in presentation.
- `gnu-emacs-presentation.org`: Project planning file.

# License

MIT. See `LICENSE`.

Emacs logo and resources are © Free Software Foundation.
